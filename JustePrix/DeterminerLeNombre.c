//
//  main.c
//  TP : Jeu
//
//  Created by Guillaume Rand on 13/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//
// TP : Jeu
//

#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main(int argc, const char * argv[]) {
    
    
    
    int continuerPartie = 1; // on définit la variable partie sur 1 pour qu'au moins une partie puisse s'effectuer
    
    while (continuerPartie == 1) { // ajout d l'option continuer partie / 1 = vrai / 0 = faux le programme d'arrete
        int choix = 0; // on initialise la variable choix
        printf("Choisissez le niveau de difficulté\n 1. entre 1 et 100\n 2. entre 1 et 1000\n 3. entre 1 et 10000\n ");
        scanf("%d", &choix); // on récupère le choix effectué par le joueur
        int nombreMaximum = 0;
        
        switch (choix) { //Suivant le choix effectué le nombre maximum change
            case 1:
                nombreMaximum = 100;
                break;
            case 2:
                nombreMaximum = 1000;
                break;
            case 3:
                nombreMaximum = 10000;
                break;
            default:
                break;
        }
        
        int choixDePartie = 0;
        long  nombreMystere = 0;
        printf("Choisissez votre type de partie :\n Type1. Nombre généré aléatoirement\n Type2. Nombre choisis par un joueur2. :");
        scanf("%d",&choixDePartie);
        
        const int MIN = 1; // on définit le minimum a 1

        switch (choix) { // on définit le type de partie
            case 1:
                srand(time(NULL)); // génération du nombre aléatoire // TODO (REPLONGER)
                   nombreMystere = (rand() % (nombreMaximum - MIN + 1)) + MIN; // intervalle du nombre aléatoire
                break;
            case 2:
                printf(" Joueur2| Choisissez le nombre mystère : \n"); // on affecte le nombreMystère choisis par le joueur 2
                scanf("%ld",&nombreMystere);
            default:
                break;
        }
    
        long nombreEntre = 0; //On définie le nombre mystère et le nombre qui seras entré
         
    
    for (int compteurDeTour = 0; nombreMystere != nombreEntre; compteurDeTour++) { // on effetcue une boucle for pour compter le nombre de tour passé
    
    while (nombreMystere != nombreEntre) { // tant que le nombre entré est différent du nombre mystère on effectue :
        compteurDeTour++; // on ajoute un tour de plus
        printf("Quel est le nombre ? : "); // la demamnde du nombre à entrer
           scanf("%ld",&nombreEntre); // on récupère le nombre entré et on l'affecte à notre variable
        
            if (nombreEntre > nombreMystere) { // si le nombre mystère  est inférieur au nombre mystère alors :
                   printf("C'est moins ! \n"); // on affiche ce texte
               } else if (nombreEntre < nombreMystere) { // si le nombre mystère est supérieur au nombre entré alors :
                   printf("C'est plus ! \n"); // on affiche ce texte
               } else if (nombreMystere == nombreEntre) { // si les deux nombre sont égaux alord
                   printf("Vous avez trouvé le bon nombre au bout de %d tour\n", compteurDeTour); // on affiche
               }
        
    }
    
}
        printf("voulez vous continuer la partie ? \n 1. Continuer \n 2. Arrêter\n");
        scanf("%d",&continuerPartie);
    }
    
    return 0;
}
