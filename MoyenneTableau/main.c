//
//  main.c
//  ExerciceMoyenneTableau
//
//  Created by Guillaume Rand on 16/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//

#include <stdio.h>
#include "main.h"

int main(int argc, const char * argv[]) {
   
    int tableau[4];
    double moyenne = 0;
    
    tableau[0] = 21;
    tableau[1] = 1;
    tableau[2] = 41;
    tableau[3] = 30;
    
    moyenne = moyenneTableau(tableau, 4);
    
    printf("la moyenne du tableau est de %f \n", moyenne);
    
    return 0;
}


double moyenneTableau(int tableau[], int tailleTableau){
    
    int i = 0, somme = 0;
    double moyenne = 0;
    
    for (i= 0 ; i < tailleTableau; i++) {
        somme = somme +tableau[i];
        
        moyenne = somme / tailleTableau;
    }

    return moyenne;
}
