/*
Jeu du pendu
Created by Guillaume Rand on 28/05/2020.
Copyright © 2020 RAND. All rights reserved.

dico.c
------

Ces fonctions piochent au hasard un mot dans un fichier dictionnaire
pour le jeu du pendu
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "dico.h"


int piocherMot(char *motPioche)
{
    FILE* dico = NULL; // Le pointeur de fichier qui va contenir notre fichier
    int nombreMots = 0, numMotChoisi = 0, i = 0;
    int caractereLu = 0;
    dico = fopen("dico.txt", "r"); // On ouvre le dictionnaire en lecture seule

    // On vÈrifie si on a rÈussi ‡ ouvrir le dictionnaire
    if (dico == NULL) // Si on n'a PAS rÈussi ‡ ouvrir le fichier
    {
        printf("\nImpossible de charger le dictionnaire de mots");
        return 0; // On retourne 0 pour indiquer que la fonction a ÈchouÈ
        // A la lecture du return, la fonction s'arrÍte immÈdiatement.
    }

    // On compte le nombre de mots dans le fichier (il suffit de compter les
    // entrÈes \n
    do
    {
        caractereLu = fgetc(dico);
        if (caractereLu == '\n')
            nombreMots++;
    } while(caractereLu != EOF);

    numMotChoisi = nombreAleatoire(nombreMots); // On pioche un mot au hasard

    // On recommence ‡ lire le fichier depuis le dÈbut. On s'arrÍte lorsqu'on est arrivÈs au bon mot
    rewind(dico);
    while (numMotChoisi > 0)
    {
        caractereLu = fgetc(dico);
        if (caractereLu == '\n')
            numMotChoisi--;
    }

    /* Le curseur du fichier est positionnÈ au bon endroit.
    On n'a plus qu'‡ faire un fgets qui lira la ligne */
    fgets(motPioche, 100, dico);

    // On vire l'\n ‡ la fin
    motPioche[strlen(motPioche) - 1] = '\0';
    fclose(dico);

    return 1; // Tout s'est bien passÈ, on retourne 1
}

int nombreAleatoire(int nombreMax)
{
    srand(time(NULL));
    return (rand() % nombreMax);
}


