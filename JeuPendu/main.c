/*
Jeu du pendu
Created by Guillaume Rand on 28/05/2020.
Copyright © 2020 RAND. All rights reserved.

main.c
------

Fonctions principales de gestion du jeu
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "dico.h"

int gagne(int lettreTrouvee[], long tailleMot);
int rechercheLettre(char lettre, char motSecret[], int lettreTrouvee[]);
char lireCaractere()
{
    char caractere = 0;

    caractere = getchar(); // On lit le premier caractËre
    caractere = toupper(caractere); // On met la lettre en majuscule si elle ne l'est pas dÈj‡

    // On lit les autres caractËres mÈmorisÈs un ‡ un jusqu'‡ l'\n
    while (getchar() != '\n') ;

    return caractere; // On retourne le premier caractËre qu'on a lu
}

int main(int argc, char* argv[])
{
    char lettre = 0; // Stocke la lettre proposÈe par l'utilisateur (retour du scanf)
    char motSecret[100] = {0}; // Ce sera le mot ‡ trouver
    int *lettreTrouvee = NULL; // Un tableau de boolÈens. Chaque case correspond ‡ une lettre du mot secret. 0 = lettre non trouvÈe, 1 = lettre trouvÈe
    long coupsRestants = 10; // Compteur de coups restants (0 = mort)
    long i = 0; // Une petite variable pour parcourir les tableaux
    long tailleMot = 0;

    printf("Bienvenue dans le Pendu !\n\n");

    if (!piocherMot(motSecret))
        exit(0);

    tailleMot = strlen(motSecret);

    lettreTrouvee = malloc(tailleMot * sizeof(int)); // On alloue dynamiquement le tableau lettreTrouvee (dont on ne connaissait pas la taille au dÈpart)
    if (lettreTrouvee == NULL)
        exit(0);

    for (i = 0 ; i < tailleMot ; i++)
        lettreTrouvee[i] = 0;

    // On continue ‡ jouer tant qu'il reste au moins un coup ‡ jouer ou qu'on
    // n'a pas gagnÈ
    while (coupsRestants > 0 && !gagne(lettreTrouvee, tailleMot))
    {
        printf("\n\nIl vous reste %ld coups a jouer", coupsRestants);
        printf("\nQuel est le mot secret ? ");

        /* On affiche le mot secret en masquant les lettres non trouvÈes
        Exemple : *A**ON */
        for (i = 0 ; i < tailleMot ; i++)
        {
            if (lettreTrouvee[i]) // Si on a trouvÈ la lettre n∞i
                printf("%c", motSecret[i]); // On l'affiche
            else
                printf("*"); // Sinon, on affiche une Ètoile pour les lettres non trouvÈes
        }

        printf("\nProposez une lettre : ");
        lettre = lireCaractere();

        // Si ce n'Ètait PAS la bonne lettre
        if (!rechercheLettre(lettre, motSecret, lettreTrouvee))
        {
            coupsRestants--; // On enlËve un coup au joueur
        }
    }


    if (gagne(lettreTrouvee, tailleMot))
        printf("\n\nGagne ! Le mot secret etait bien : %s", motSecret);
    else
        printf("\n\nPerdu ! Le mot secret etait : %s", motSecret);

    free(lettreTrouvee); // On libËre la mÈmoire allouÈe manuellement (par malloc)

        return 0;
}





int gagne(int lettreTrouvee[], long tailleMot)
{
    long i = 0;
    int joueurGagne = 1;

    for (i = 0 ; i < tailleMot ; i++)
    {
        if (lettreTrouvee[i] == 0)
            joueurGagne = 0;
    }

    return joueurGagne;
}

int rechercheLettre(char lettre, char motSecret[], int lettreTrouvee[])
{
    long i = 0;
    int bonneLettre = 0;

    // On parcourt motSecret pour vÈrifier si la lettre proposÈe y est
    for (i = 0 ; motSecret[i] != '\0' ; i++)
    {
        if (lettre == motSecret[i]) // Si la lettre y est
        {
            bonneLettre = 1; // On mÈmorise que c'Ètait une bonne lettre
            lettreTrouvee[i] = 1; // On met ‡ 1 le case du tableau de boolÈens correspondant ‡ la lettre actuelle
        }
    }

    return bonneLettre;
}
