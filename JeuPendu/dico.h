/*
Jeu du pendu
Created by Guillaume Rand on 28/05/2020.
Copyright © 2020 RAND. All rights reserved.

dico.h
------

Contient les prototypes des fonctions de dico.c
*/


#ifndef DEF_DICO
#define DEF_DICO


int piocherMot(char *motPioche);
int nombreAleatoire(int nombreMax);

#endif

