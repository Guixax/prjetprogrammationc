//
//  main.c
//  LesPointeurs
//
//  Created by Guillaume Rand on 15/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//
// Lorsque l'on inscrit une valeur dans la RAM, il est possible d'afficher l'adresse correspondante :
//printf("L'adresse de la variable age est : %p", &age);
// les variable faite pour dontenir des adresses (RAM) sont des pointeurs, ces valeurs indiquent l'adresse d'une valueur en mémoire
//pointeur : int *pointeur;

#include <stdio.h>
#include "main.h"


int main(int argc, const char * argv[]) {
    
//    int age = 10;
//    int *pointeurSurAge; // 1) signifie "Je crée un pointeur"
//    pointeurSurAge = &age; // 2) signifie "pointeurSurAge contient l'adresse de la variable age"
//    printf("%d\n", *pointeurSurAge);
//    printf("%d\n", pointeurSurAge);
//    // OU
//    //int *pointeurSurAge = &age;
//    // le type devant un pointeur sert à indiquer quelle est le type de valeur contenu dans l'adresse
    
    printf("Convertisseur de minutes en heure et minutes\n");
    
    int heures = 0, minutes = 0; // on initialise les variables de minutes et d'heure
    
    
    printf("Entrez les minutes : ");
    scanf("%d",&minutes); // Entrez la valeur des minutes


    decoupeMinute(&heures, &minutes); // on applique la fonctions sur les deux valeurs // On envoie l'adresse des deux variables dans la focntion
    
    printf("\n%d heures et %d minutes\n\n", heures, minutes);

    return 0;
}

// Fonction sans return(void) : prend en paramètre *pointeurMinutes = Valeur de la variable minutes fait à partir de l'adresse envoyé // *pointeurHeures = Valeur de la varible heure fait à partir de l'adresse envoyé
void decoupeMinute(int *pointeurHeures,int *pointeurMinutes) // attention à l'ordre de déclaration des variables
{
    
    *pointeurHeures = *pointeurMinutes / 60;  // on effectue les calcul de conversion heure et minute
    *pointeurMinutes = *pointeurMinutes % 60; // on utilise le modulo pour récupérer le reste des minutes de la première opérations
    
}
