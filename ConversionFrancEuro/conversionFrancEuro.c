//
//  main.c
//  Fonctions
//
//  Created by Guillaume Rand on 13/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//
// 
//
// Conversion euro en Franc et franc euro

#include <stdio.h>

double conversionFranc(double euro) //Fonction permettant la conversion euro -> francs
{
    
       return 6.55957 * euro;
}
double conversionEuro(double euro) //Fonction permettant la convertion francs -> euro
{
    
       return euro / 6.55957;
}


int main(int argc, const char * argv[]) {

    int choix = 0; // on définit un système de choix pour le type de conversion souhaiter
    printf("1. conversion Franc -> Euro\n2. conversion Euro -> Franc\n");
    scanf("%d", &choix);
    if (choix ==1) { // Conversion Franc - > euro
        
        float franc = 0; 
         int euro = 0;
         
         printf("entrez un montant en francs :");
         scanf("%f", &franc); //on récupère le montant en franc donner par l'user
        
         euro = conversionEuro(franc); // on effectue la conversion
         printf("%f francs donne %d euros \n",franc,euro); // 8 // on affiche le resultat
        
        
    } else if(choix == 2){ // conversion Euro - > Franc
        
        int euro = 0; //
         double franc = 0;
         
         printf("entrez un montant en euro :"); 
         scanf("%d", &euro); // on récupère le montant en euro donner par l'user
        
         franc = conversionFranc(euro); //on effectue la conversion
         printf("%d euro donne %f Franc \n",euro,franc); // on affiche le resultat
        
    } else {
        printf("Erreur");
    }
    
    
    //OU
    // Le résultat de la fonction est directement envoyé au printf et n'est pas stocké dans une variable
    // printf("Le triple de ce nombre est %d\n", triple(nombreEntre));
     
    
        return 0; 
}
