//
//  main.h
//  ExerciceCopieTableau
//
//  Created by Guillaume Rand on 16/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//

#ifndef main_h
#define main_h
void copie(int tableauOriginal[], int tableauCopie[], int tailleTableau);

#endif /* main_h */
