//
//  main.c
//  ExerciceCopieTableau
//
//  Created by Guillaume Rand on 16/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//

#include <stdio.h>
#include "main.h"

int main(int argc, const char * argv[]) {

    int tableauOriginal[4], tableauCopie[4] = {0,0,0,0}; //initialisation des variables
    
    tableauOriginal[0] = 10;
    tableauOriginal[1] = 11;
    tableauOriginal[2] = 12;
    tableauOriginal[3] = 13;
    
    copie(tableauOriginal, tableauCopie, 4); // application de la focntion
    
    return 0;
}

void copie(int tableauOriginal[], int tableauCopie[], int tailleTableau)
{
    
    int i = 0;
    
    for (i = 0; i < tailleTableau; i++) { // on affecte chaque valeur du TableauOriginal au TableauCopie
        tableauCopie[i] = tableauOriginal[i];
    }
    
    printf("Le tableau copier\n");
    for (i= 0; i < tailleTableau; i++) { // on affiche chaque valeurs du tableau copier
        
    printf("%d\n", tableauCopie[i]);
    
    }
}
