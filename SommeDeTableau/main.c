//
//  main.c
//  ExerciceTableauxPointeurs
//
//  Created by Guillaume Rand on 15/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//
//Créez une fonctionsommeTableauqui renvoie la somme des valeurs contenues dans le
//tableau (utilisez unreturnpour renvoyer la valeur). Pour vous aider, voici le
//prototype de la fonction à créer.

#include <stdio.h>
#include "main.h"

int main(int argc, const char * argv[]) {
 

    int tableau[4], SommeDuTableau = 0; // initailisation des variables et du tableau
    
    tableau[0]= 34; //Définition des valeurs de chaque case du tableau
    tableau[1]= 50;
    tableau[2]= 6;
    tableau[3]= 93;
    
    SommeDuTableau = somme(tableau, 4); // on envoie le résultat de la somme du tableau dans une variable
    
    printf("La somme du tableau est de %d\n", SommeDuTableau);
    
    return 0;
}

int somme(int tableau[], int tailleTableau){
    
    int i = 0, sommeTableau = 0; // on initilise les variables
    
    for (i = 0; i < tailleTableau; i++){ // on effectue le calcul
        
    sommeTableau = sommeTableau + tableau[i];
    
    }
    return sommeTableau;
}
