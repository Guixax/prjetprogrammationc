//
//  main.c
//  ExerciceMaximumTableau
//
//  Created by Guillaume Rand on 16/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//

#include <stdio.h>
#include "main.h"

int main(int argc, const char * argv[]) {
    
    int tableau[4], valeurMax = 0; // initilisation des variables
    
    tableau[0] = 10;
    tableau[1] = 20;
    tableau[2] = 30;
    tableau[3] = 40;
    
    for (int i= 0; i < 4; i++) { //on affiche toute les valeurs du tableau
        printf("%d\n", tableau[i]);
    }
    
    printf("Entrez votre la valeur max :");
    scanf("%d\n",&valeurMax); // on entre la valeur max
    
    
    maximumTableau(tableau,4 , valeurMax); // on applique la fonction
    
    printf("TABLEAU RÉINITIALISÉ\n");
    for (int i= 0; i < 4; i++) { // Puis on affiche le tableau réinitialisé avec la valeur Max
        printf("%d\n", tableau[i]);
    }

    return 0;
}


void maximumTableau(int tableau[], int tailleTableau, int valeurMax)
{

    for (int i = 0; i < tailleTableau; i++) {
        if (tableau[i] > valeurMax) { // Si une des valeurs du tableau est supérieur à la valeurMAx prédéfinie
            tableau[i] = 0;
        }
    
        }
    }
    

