//
//  main.h
//  ExerciceMaximumTableau
//
//  Created by Guillaume Rand on 16/05/2020.
//  Copyright © 2020 RAND. All rights reserved.
//

#ifndef main_h
#define main_h
void maximumTableau(int tableau[], int tailleTableau, int valeurMax);

#endif /* main_h */
